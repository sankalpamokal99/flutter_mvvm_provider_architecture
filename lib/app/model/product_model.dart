// To parse this JSON data, do
//
//     final product = productFromJson(jsonString);

import 'dart:convert';

Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product({
    required this.image,
    required this.name,
    required this.price,
    required this.sId,
    required this.total,
  });

  String image;
  String name;
  int price;
  String sId;
  int total;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        image: json["image"] == null ? null : json["image"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        sId: json["sId"] == null ? null : json["sId"],
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "image": image == null ? null : image,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "sId": sId == null ? null : sId,
        "total": total == null ? null : total,
      };
}
