import 'dart:io';

import 'package:dio/dio.dart';
import 'package:mvvm_provider_architecture/app/model/product_model.dart';

class ApiService {
  Dio? dio;

  ApiService() {
    dio = Dio(BaseOptions(
        baseUrl:
            Platform.isIOS ? "http://localhost:3000" : "http://10.0.2.2:3000"));
  }

  Future<List<Product>> getAllProducts() async {
    // final response = await dio.get("/products");
    //
    // if (response.statusCode == HttpStatus.ok) {
    //   final responseBody = response.data as List;
    //
    //   return responseBody.map((e) => Product.fromJson(e)).toList();
    // }
    return [
      Product(
          image:
              "https://image.shutterstock.com/image-illustration/red-stamp-on-white-background-260nw-1165179109.jpg",
          name: "Mobile",
          price: 2000,
          sId: "1",
          total: 2000),
      Product(
          image:
              "https://image.shutterstock.com/image-illustration/red-stamp-on-white-background-260nw-1165179109.jpg",
          name: "TV",
          price: 250,
          sId: "2",
          total: 250),
      Product(
          image:
              "https://image.shutterstock.com/image-illustration/red-stamp-on-white-background-260nw-1165179109.jpg",
          name: "Cooler",
          price: 1000,
          sId: "3",
          total: 1000),
      Product(
          image:
              "https://image.shutterstock.com/image-illustration/red-stamp-on-white-background-260nw-1165179109.jpg",
          name: "Wardrobe",
          price: 3000,
          sId: "4",
          total: 3000),
    ];

    throw Exception();
  }

  Future<bool> addProduct(Product product) async {
    final response = await dio!.post("/product",
        data: product.toJson(),
        options: Options(headers: {"user-id": "5fbd48380fe34c4b213dfe6b"}));

    if (response.statusCode == HttpStatus.ok) {
      return true;
    }

    return false;
  }
}
