import 'package:flutter/material.dart';
import 'package:mvvm_provider_architecture/app/model/product_model.dart';
import 'package:mvvm_provider_architecture/app/services/ApiService.dart';

class ProductsViewModel with ChangeNotifier {
  ProductsViewModel() {
    paginationDemo ? fetchItemsList() : getAllProducts();
    scrollController.addListener(() {
      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent) {
        print("new data call");
        fetchItemsList();
      }
    });
  }
  bool listEnded = false;
  bool isFetching = false;
  bool paginationDemo = true;
  List<String> items = [];
  List<Product> products = [];

  ScrollController scrollController = ScrollController();

  // @override
  // void initState() {
  //   super.initState();
  //   getAllProducts();
  // }
  fetchItemsList() {
    isFetching = true;
    Future.delayed(Duration(seconds: 2), () {
      List<String> newData = items.length >= 60
          ? []
          : List.generate(20, (index) => "List Item ${index + items.length}");
      if (newData.isNotEmpty) {
        items.addAll(newData);
      } else {
        print("no more data");
        listEnded = true;
      }
      isFetching = false;
      notifyListeners();
    });
  }

  Future<void> getAllProducts() async {
    print("getallproducts called");
    products = await ApiService().getAllProducts();
    notifyListeners();
  }

  Map<Product, int> basketProducts = {};

  List<Product> get basketItems => basketProducts.keys.toList();

  double get basketTotalMoney {
    if (basketProducts.isEmpty) {
      return 0;
    } else {
      double _total = 0;
      basketProducts.forEach((key, value) {
        _total += key.price * value;
      });
      return _total;
    }
  }

  int get totalProduct {
    return basketProducts.length;
  }

  void addFirstItemToBasket(Product product) {
    basketProducts[product] = 1;
    ApiService().addProduct(product);
    notifyListeners();
  }

  void incrementProduct(Product product) {
    if (basketProducts[product] == null) {
      addFirstItemToBasket(product);
      return;
    } else
      basketProducts[product] = basketProducts[product]! + 1;
    notifyListeners();
  }

  void decrementProduct(Product product) {
    if (basketProducts[product] == null) return;
    if (basketProducts[product] == 1) {
      basketProducts.removeWhere((key, value) => key == product);
    } else {
      basketProducts[product] = basketProducts[product]! - 1;
    }
    notifyListeners();
  }
}
