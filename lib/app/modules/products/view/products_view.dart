import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvvm_provider_architecture/app/model/product_model.dart';
import 'package:mvvm_provider_architecture/app/modules/products/viewmodel/products_viewmodel.dart';
import 'package:provider/provider.dart';

class ProductsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [buildActionChipTotalMoney()]),
      body: context.read<ProductsViewModel>().paginationDemo
          ? paginatedListView(context)
          : Consumer<ProductsViewModel>(
              builder: (_, productsModel, child) => ListView.builder(
                itemCount: productsModel.products.length,
                itemBuilder: (context, index) {
                  return ShopCard(product: productsModel.products[index]);
                },
              ),
            ),
    );
  }

  Widget buildActionChipTotalMoney() {
    return Consumer<ProductsViewModel>(
        builder: (_, productsModel, child) => ActionChip(
              avatar: Icon(Icons.check, color: Colors.green),
              label: Text("${productsModel.basketTotalMoney} TL"),
              onPressed: () {
                productsModel.getAllProducts();
              },
            ));
  }

  Widget paginatedListView(BuildContext context) {
    return Consumer<ProductsViewModel>(builder: (_, productsModel, child) {
      return (productsModel.items.isNotEmpty)
          ? Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    controller: productsModel.scrollController,
                    itemCount: productsModel.items.length +
                        (productsModel.listEnded ? 1 : 0),
                    itemBuilder: (context, index) {
                      if (index < productsModel.items.length) {
                        return ListTile(
                          title: Text(productsModel.items[index]),
                        );
                      } else {
                        return Container(
                          height: 50,
                          child: Center(child: Text("No More Data")),
                        );
                      }
                    },
                  ),
                ),
                productsModel.isFetching
                    ? Text("value true")
                    : Text("value false")
              ],
            )
          : Center(child: CircularProgressIndicator());
    });
  }
}

class ShopCard extends StatelessWidget {
  final Product? product;

  const ShopCard({Key? key, @required this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        trailing: buildIconButtonAdd(context),
        title: buildSizedBoxImage(context),
        subtitle: buildWrapSub(),
      ),
    );
  }

  IconButton buildIconButtonAdd(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.shopping_basket),
      onPressed: () {
        Provider.of<ProductsViewModel>(context, listen: false)
            .addFirstItemToBasket(product!);
      },
    );
  }

  Widget buildSizedBoxImage(BuildContext context) {
    return Column(
      children: [
        SizedBox(
            height: MediaQuery.of(context).size.height * 0.2,
            child: Image.network(product!.image)),
        SizedBox(height: MediaQuery.of(context).size.height * 0.01),
        buildContainerItem(context)
      ],
    );
  }

  Container buildContainerItem(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          buildIconButtonIncrement(context),
          Text(
              "${context.watch<ProductsViewModel>().basketProducts[product] ?? 0}"),
          buildIconButtonRemove(context),
        ],
      ),
    );
  }

  IconButton buildIconButtonIncrement(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.add),
      onPressed: () {
        Provider.of<ProductsViewModel>(context, listen: false)
            .incrementProduct(product!);
      },
    );
  }

  IconButton buildIconButtonRemove(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.remove),
      onPressed: () {
        Provider.of<ProductsViewModel>(context, listen: false)
            .decrementProduct(product!);
      },
    );
  }

  Wrap buildWrapSub() {
    return Wrap(
      spacing: 10,
      children: [
        Text(product!.name),
        Text("${product!.price}"),
      ],
    );
  }
}
