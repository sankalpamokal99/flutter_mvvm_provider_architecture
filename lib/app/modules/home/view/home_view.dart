import 'package:flutter/material.dart';
import 'package:mvvm_provider_architecture/app/modules/basket/view/basket_view.dart';
import 'package:mvvm_provider_architecture/app/modules/products/view/products_view.dart';
import 'package:mvvm_provider_architecture/app/modules/products/viewmodel/products_viewmodel.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          bottomNavigationBar: buildBottomAppBar(context),
          body: TabBarView(children: [
            ProductsView(),
            BasketView(),
          ]),
        ));
  }

  BottomAppBar buildBottomAppBar(BuildContext context) {
    return BottomAppBar(
        elevation: 20,
        child: TabBar(
          labelPadding: EdgeInsets.zero,
          indicatorPadding: EdgeInsets.zero,
          tabs: [
            Tab(icon: Icon(Icons.home)),
            Tab(
              icon: Stack(
                children: [
                  Icon(Icons.shopping_basket),
                  Positioned(
                      top: 0,
                      right: 0,
                      height: 10,
                      width: 10,
                      child: buildCircleAvatarCounter(context)),
                ],
              ),
            )
          ],
          labelColor: Colors.black,
        ));
  }

  CircleAvatar buildCircleAvatarCounter(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Colors.red,
      child: Consumer<ProductsViewModel>(
        builder: (_, productsModel, child) => Text(
            productsModel.totalProduct.toString(),
            style: Theme.of(context)
                .textTheme
                .overline!
                .copyWith(color: Colors.white)),
      ),
    );
  }
}
