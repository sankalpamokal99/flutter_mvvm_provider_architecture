import 'package:flutter/material.dart';
import 'package:mvvm_provider_architecture/app/modules/products/viewmodel/products_viewmodel.dart';
import 'package:provider/provider.dart';

class BasketView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Expanded(child: buildListViewBaskets(context)),
          _minumumButton(context),
        ],
      ),
    );
  }

  Widget buildListViewBaskets(BuildContext context) {
    return Consumer<ProductsViewModel>(
      builder: (_, productsModel, child) => ListView.builder(
        itemCount: productsModel.basketItems.length,
        itemBuilder: (context, index) {
          final product = productsModel.basketItems[index];
          return Column(
            children: [
              Image.network(product.image),
              Text(
                  "${productsModel.basketProducts[product]} * ${product.price}"),
            ],
          );
        },
      ),
    );
  }

  Widget _minumumButton(BuildContext context) {
    return OutlinedButton(
      child: Text("Minumum 100 TL"),
      onPressed:
          // context.watch<User>().basketTotalMoney > 100
          false ? () {} : null,
    );
  }
}
